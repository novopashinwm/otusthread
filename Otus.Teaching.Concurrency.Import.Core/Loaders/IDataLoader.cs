﻿
namespace Otus.Teaching.Concurrency.Import
{
    public enum SelectDb { SQLServer, SQLite }
    public interface IDataLoader
    {
        void LoadData(int numberOfThreads, SelectDb selectDB);
    }
}