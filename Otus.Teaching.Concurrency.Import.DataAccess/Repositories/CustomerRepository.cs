using System;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository, IDisposable
    {
        private MS_SQLContext mySQL;

        private SQLiteContext myLite;

        private bool disposed = false;

        public CustomerRepository(SelectDb selectionSQL)
        {
            if (selectionSQL == SelectDb.SQLServer)
                mySQL = new MS_SQLContext();
            else
                myLite = new SQLiteContext();
        }

        public void AddCustomer(Customer customer, SelectDb selectionSQL)
        {
            if (selectionSQL == SelectDb.SQLServer)
                mySQL.Customers.Add(customer);
            else
                myLite.Customers.Add(customer);
        }

        public void AddCustomers(IEnumerable<Customer> customers, SelectDb selectionSQL)
        {
            if (selectionSQL == SelectDb.SQLServer)
                mySQL.Customers.AddRange(customers);
            else
                myLite.Customers.AddRange(customers);
        }

        public void Save(SelectDb selectionSQL)
        {
            if (selectionSQL == SelectDb.SQLServer)
                mySQL.SaveChanges();
            else
                myLite.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                mySQL?.Dispose();

                myLite?.Dispose();
            }

            disposed = true;
        }
    }
}