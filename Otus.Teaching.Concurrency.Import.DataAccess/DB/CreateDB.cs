﻿using Otus.Teaching.Concurrency.Import.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import {


    public class CreateDB
    {
        private readonly MS_SQLContext _ms_sql;
        private readonly SQLiteContext _sqlite;        

        private SelectDb _selectionSQL;

        public CreateDB(SelectDb selectionSQL, object dataContext)
        {
            _selectionSQL = selectionSQL;

            if (selectionSQL == SelectDb.SQLServer)
                _ms_sql = (MS_SQLContext)dataContext;
            else
                _sqlite = (SQLiteContext)dataContext;
        }

        public void Init()
        {
            if (_selectionSQL == SelectDb.SQLServer)
            {
                _ms_sql.Database.EnsureDeleted();
                _ms_sql.Database.EnsureCreated();
            }
            else
            {
                _sqlite.Database.EnsureDeleted();
                _sqlite.Database.EnsureCreated();
            }
        }
    }
}
