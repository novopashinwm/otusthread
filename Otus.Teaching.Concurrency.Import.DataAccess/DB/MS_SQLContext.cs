﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class MS_SQLContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connStr = "Server=(localdb)\\mssqllocaldb; " +
                "Database=Otus.Teaching.Concurrency.Import.DataAccessdb; " +
                "Trusted_Connection=True; Pooling = true";
            optionsBuilder.UseSqlServer(connStr);
        }
    
    }
}
