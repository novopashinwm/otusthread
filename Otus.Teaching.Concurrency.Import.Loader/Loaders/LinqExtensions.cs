﻿using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    static class LinqExtensions
    {
        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> items, int numOfParts)
        {
            int i = 0;

            return items.GroupBy(x => i++ % numOfParts);
        }

        public static IEnumerable<IEnumerable<Customer>> Portion(this IEnumerable<Customer> source, int portionSize)
        {
            if (portionSize < 1)
            {
                throw new ArgumentException("Portion size must be greater than 0.");
            }

            var enumerator = source.GetEnumerator();

            while (enumerator.MoveNext())
            {
                yield return Receiver(enumerator.Current, enumerator, portionSize);
            }
        }

        private static IEnumerable<Customer> Receiver(Customer first, IEnumerator<Customer> end, int size)
        {
            while (true)
            {
                yield return first;

                if (--size == 0)
                {
                    break;
                }

                if (end.MoveNext())
                {
                    first = end.Current;
                }
                else
                {
                    break;
                }
            }
        }

        public static void GetDBCountItems(SelectDb selection)
        {
            if (selection == SelectDb.SQLServer)
            {
                using (MS_SQLContext server = new MS_SQLContext())
                {
                    Console.WriteLine($"The number of elements in the database SQL Server: {server.Customers.Count()}");
                }
            }
            else
            {
                using (SQLiteContext lite = new SQLiteContext())
                {
                    Console.WriteLine($"The number of elements in the database SQLite: {lite.Customers.Count()}");
                }
            }
        }
    }
}
