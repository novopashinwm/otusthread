﻿
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import
{
    public class DataLoader : IDataLoader
    {
        private XmlParser xmlParser;
        private List<Customer> listCustomers;
        private SelectDb _selectionSQL;
        private readonly List<Thread> Threads = new List<Thread>();

        public DataLoader()
        {
            xmlParser = new XmlParser();
            listCustomers = xmlParser.Parse();
        }

        public void LoadData(int numberOfThreads, SelectDb selectionSQL)
        {
            _selectionSQL = selectionSQL;
            int threadsSize = (listCustomers.Count + numberOfThreads - 1) / numberOfThreads;
            var subCollectionsSize = LinqExtensions.Split(listCustomers, numberOfThreads);
            Console.WriteLine("Started loading data...");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            foreach (var items in subCollectionsSize)
            {
                Threads.Add(ThreadJob(items));
            }

            Threads.ForEach(x => x.Start());
            Threads.ForEach(x => x.Join());
            stopwatch.Stop();
            Console.WriteLine($"Loaded data in {stopwatch.Elapsed}...");
            LinqExtensions.GetDBCountItems(_selectionSQL);
        }

        private Thread ThreadJob(IEnumerable<Customer> collection)
        {
            var accelerator = new Accelerator(collection, _selectionSQL);
            return new Thread(accelerator.SortOut);
        }
    }
}
