﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.NetworkInformation;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class MyGenerator
    {
        public static int count;
        private static string prefix = "Otus.Teaching.Concurrency.Import";
        private static string ProcessFileName =  $"{prefix}.DataGenerator.App.exe";
        private string ProcessDirectory = @$"D:\Homework\{prefix}\{ProcessFileName}\bin\Debug\netcoreapp3.1";
        private string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");

        private Process process;
        enum Run { Method, Process }
        private Run run = Run.Method;

        public void GetDataFile(string[] args) 
        {
            GetFileAndMethod(args);

            if (run== Run.Method)
            {
                var xmlGenerator = new XmlGenerator(path, count);
                xmlGenerator.Generate();
            }
            else
            {
                GenerateProcess();
                process.WaitForExit();
                process.Kill();
            }
        }

        private void GenerateProcess()
        {
            var startInfo = new ProcessStartInfo()
            {
                Arguments = path,
                FileName = Path.Combine(ProcessDirectory, ProcessFileName)
            };
            Process.Start(startInfo);
        }

        private void GetFileAndMethod(string[] args)
        {
            if (args != null && args.Length == 2)
            {
                path = args[0];
                run = (Run) Enum.Parse(typeof(Run), args[1]);
            }
        }
    }
}
