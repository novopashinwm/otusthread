﻿using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class Accelerator
    {
        private SelectDb _selectDB;

        public const int PortionSize = 5000;

        private readonly IEnumerable<Customer> _customers;

        public Accelerator(IEnumerable<Customer> customers, SelectDb selectionSQL)
        {
            _customers = customers;

            _selectDB = selectionSQL;
        }

        public void SortOut()
        {
            var portions = _customers.Portion(PortionSize);

            foreach (var portion in portions)
            {
                using var repository = new CustomerRepository(_selectDB);

                repository.AddCustomers(portion, _selectDB);

                AttemptsToSave(repository);
            }
        }

        private void AttemptsToSave(CustomerRepository repository)
        {
            int Counter = 0;
            do
            {
                try
                {
                    repository.Save(_selectDB);

                    break;
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc);

                    Counter++;
                }
            }
            while (Counter < 50);
        }
    }
}
