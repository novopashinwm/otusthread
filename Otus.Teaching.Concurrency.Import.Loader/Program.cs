﻿using System;
using System.Diagnostics;
using System.IO;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;


namespace Otus.Teaching.Concurrency.Import
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        private static SelectDb selectDb = SelectDb.SQLite;

        static void Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            GenerateCustomersDataFile();
            InitDb(selectDb);

            var loader = new DataLoader();
            int numberOfThreads = Environment.ProcessorCount;
            loader.LoadData(numberOfThreads, selectDb);
            Console.ReadLine();
        }

        private static void InitDb(SelectDb selectDb)
        {
            if (selectDb == SelectDb.SQLServer)
            {
                using var sql = new MS_SQLContext();
                new CreateDB(selectDb, sql).Init();
            }
            else
            {
                using var sqLite = new SQLiteContext();
                new CreateDB(selectDb, sqLite).Init();
            }
        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 1000);
            xmlGenerator.Generate();
        }
    }
}